# Next.js Include React Package Demo

> This demo showcases how `styled-components` is throwing a weird error when being included as a package.

## Setup

```bash
$ git clone git@bitbucket.org:neogeek/nextjs-include-react-package-demo.git
$ make install
```

## Run

First run the demo in it's working state.

```bash
$ make run
```

Then open `react-components/srcjs/Header.jsx` and change the contents to:

```javascript
import { StyledHeader } from "./Header.styles";

export default function Header({ children }) {
  return <StyledHeader>{children}</StyledHeader>;
}
```

And then rerun the test.

```bash
$ make run
```

You should see the following error:

![](screenshots/error.jpg)

😭

Also, if you copy the `bundle.min.js` into the `nextjs-demo/` folder and use this as the import ...

```javascript
import { Header } from "../bundle.min.js";
```

... it's fine.

## References

- https://github.com/vercel/next.js/issues/7626
- https://github.com/jaredpalmer/tsdx/issues/950
- https://stackoverflow.com/questions/58365151/hooks-error-invalid-hook-call-using-nextjs-or-reactjs-on-windows
