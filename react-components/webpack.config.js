module.exports = {
    entry: './src/js/index.jsx',
    module: {
        rules: [
            {
                test: /\.jsx?$/u,
                include: /.*/,
                exclude: /node_modules/u,
                use: {
                    loader: 'babel-loader',
                    options: {
                        plugins: ['babel-plugin-styled-components'],
                        presets: [
                            '@babel/preset-env',
                            [
                                '@babel/preset-react',
                                {
                                    runtime: 'automatic'
                                }
                            ]
                        ]
                    }
                }
            }
        ]
    },
    output: {
        filename: 'bundle.min.js',
        path: `${process.cwd()}/dist`,
        library: {
            type: 'commonjs2'
        }
    },
    externals: {
        react: 'react',
        reactDOM: 'react-dom',
        styledComponents: 'styled-components'
    },
    resolve: {
        extensions: ['.js', '.jsx']
    }
};
