import { StyledHeader } from './Header.styles';

export default function Header({ children }) {
    return <h1>{children}</h1>;
}
