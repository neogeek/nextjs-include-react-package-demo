install:
	(cd react-components && npm ci)
	(cd nextjs-demo && npm ci)

run:
	(cd react-components && npm run build)
	(cd nextjs-demo && npm start)

clean:
	git clean -xdf
